//
//  SecondaryViewController.swift
//  Lab2
//
//  Created by Zhanat on 05.09.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import UIKit

class SecondaryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var resultLabel: UILabel!
    
    var result = Question.sharedInstance.res;
    var answers = Question.sharedInstance.answers;
    var quiz = [Quiz]()
    //var questNumber = 0;
    //var questions = Question.sharedInstance.questions;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultLabel.text = String(result)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.alwaysBounceVertical = true
        //tableView.register(ResultTableViewCell.self, forCellReuseIdentifier: "resultCell")
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath) as! ResultTableViewCell
        
        cell.questionLabel.text = quiz[indexPath.row].question
        cell.rightAnswerLabel.text = quiz[indexPath.row].answer
        cell.yourAnswerLabel.text = answers[indexPath.row]
        
        if (cell.rightAnswerLabel.text == cell.yourAnswerLabel.text){
            cell.backgroundColor = UIColor.green
        } else {
            cell.backgroundColor = .red
        }
        return cell
        
    }
    
    @IBAction func startQuizBtnTpd(_ sender: Any) {
        let _: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : ViewController = storyboard?.instantiateViewController(withIdentifier: "quizVC") as! ViewController
        vc.res = 0
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
