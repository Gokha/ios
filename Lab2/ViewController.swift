//
//  ViewController.swift
//  Lab2
//
//  Created by Zhanat on 28.08.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var btnsView: UIView!
    var quiz = [Quiz]()
    
    var choosenAnswers = Question.sharedInstance.answers;
    
    var btnLocationY = 0;
    var questNumb = 0;
    var startTag = 0;
    var res = 0;
    var btnTag = 0;
    var btnDistance = 60;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        quiz = Singleton.sharedInstance.getTheQuiz()
        
        questionLabel.text = quiz[questNumb].question
        
        if (questNumb != 0)
        {
            startTag = quiz[questNumb-1].variants.count;
            for delete in 0...startTag {
                let deleteBtn = self.view.viewWithTag(delete) as? UIButton;
                deleteBtn?.removeFromSuperview();
            }
        
        }
        btnTag = 0;
        
        for variant in quiz[questNumb].variants
        {
            let btn = UIButton(frame: CGRect(x: 120, y: btnLocationY, width: 150, height: 50));
            btn.tag = btnTag;
            btn.backgroundColor = .orange;
            btn.setTitle(String(describing: variant), for: .normal)
            btn.addTarget(self, action:#selector(btnTapped), for: .touchUpInside)
            btnLocationY += btnDistance;
            btnTag += 1;
            self.btnsView.addSubview(btn)
        }
        
    }
    
    func btnTapped(sender: UIButton){
        if(quiz[questNumb].variants[sender.tag] == quiz[questNumb].answer){
            res += 1;
        }
        choosenAnswers.append(quiz[questNumb].variants[sender.tag])
        questNumb += 1;
        
        //if(transitionCoordinator==horizontall)
        btnLocationY = 0;
        
        if(questNumb == quiz.count){
            print(res);
            Question.sharedInstance.res = res;
            Question.sharedInstance.answers = choosenAnswers;
            let myVC = storyboard?.instantiateViewController(withIdentifier: "SecondaryViewController") as! SecondaryViewController
            myVC.quiz = quiz
            navigationController?.pushViewController(myVC, animated: true)
            //getResult();
        }
        else{
            viewDidLoad();
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

