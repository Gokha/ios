//
//  Quiz.swift
//  Lab2
//
//  Created by Zhanat on 22.09.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import Foundation
import UIKit

class Quiz: NSObject{
    
    var question: String
    var variants = [String]()
    var answer: String
    
    
    init(question: String, variants: [String], answer: String) {
        
        self.question = question
        self.variants = variants
        self.answer = answer
    }
}
