//
//  Singleton.swift
//  Lab2
//
//  Created by Zhanat on 22.09.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import Foundation

class Singleton : NSObject {
    static let sharedInstance = Singleton()
    
    var questions:[String:String]=[
    "How many oscars did the Titanic movie got?":"12",
    "How long is an Olympic swimming pool?":"50 meters",
    "How many minutes is a rugby match?":"80 minutes",
    "What is the house number of the Simpsons?":"Number 742"
    ]
    var variants:[[String]]=[
    ["12","13","14","15"],
    ["50 minutes","60minutes","70 minutes","80 minutes"],
    ["30 meters","40 meters","50 meters"],
    ["Number 742","Number 746","Number 734"]
    ]
    
    func getTheQuiz() -> [Quiz]{
        var quizArray = [Quiz]()
        for _ in questions{
            let r = Int(arc4random_uniform(UInt32(questions.count)))
            let question = Array(questions)[r].key
            let answer = Array(questions)[r].value
            let variant = variants[r]
            let quizModel = Quiz(question: question, variants: variant, answer: answer)
            quizArray.append(quizModel)
        }
        return quizArray
        
        
    }
}
