//
//  Question.swift
//  Lab2
//
//  Created by Zhanat on 20.09.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

import Foundation

class Question {
    static let sharedInstance = Question()
    var res=0;
    var answers :[String] = [];
    var questions:[String:String]=[:];
    var variants : [[String]] = [];
    private init(){
        res = 0;
        answers = [];
        questions=[
            "How many oscars did the Titanic movie got?":"12",
            "How long is an Olympic swimming pool?":"50 meters",
            "How many minutes is a rugby match?":"80 minutes",
            "What is the house number of the Simpsons?":"Number 742"
        ];
        variants=[
            ["12","13","14","15"],
            ["50 minutes","60minutes","70 minutes","80 minutes"],
            ["30 meters","40 meters","50 meters"],
            ["Number 742","Number 746","Number 734"]
        ];
        
    }
}
